package com.twuc.webApp.simplest.scanning;

import com.twuc.webApp.simplest.scanning.Dependent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class WithDependency {
    @Autowired
    private Dependent dependent;


    public Dependent getDependent() {
        return dependent;
    }
}
