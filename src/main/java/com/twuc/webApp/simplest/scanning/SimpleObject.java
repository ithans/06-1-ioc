package com.twuc.webApp.simplest.scanning;

import com.twuc.webApp.simplest.scanning.SimpleDependent;
import com.twuc.webApp.simplest.scanning.SimpleInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class SimpleObject implements SimpleInterface {
    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }

    private SimpleDependent simpleDependent;



    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }
}
