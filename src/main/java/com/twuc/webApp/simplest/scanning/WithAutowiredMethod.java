package com.twuc.webApp.simplest.scanning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private AnotherDependent anotherDependent;

    @Autowired
    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
    }
}
