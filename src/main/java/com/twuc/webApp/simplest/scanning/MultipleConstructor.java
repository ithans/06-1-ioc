package com.twuc.webApp.simplest.scanning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    public Dependent getDependent() {
        return dependent;
    }

    private Dependent dependent;

    public String getMessage() {
        return message;
    }

    private String message;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(String message) {
        this.message = message;
    }

    public MultipleConstructor() {
    }
}
