package com.twuc.webApp.yourTurn;

import com.twuc.webApp.simplest.out.OutOfScanningScope;
import com.twuc.webApp.simplest.scanning.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class SpringbootTest {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.simplest.scanning");

    @Test
    void test_without_dependency() {
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
    }

    @Test
    void test_within_dependency() {
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean);
        assertNotNull(bean.getDependent());
    }
    void test_is_or_not_equals() {
        WithDependency bean1 = context.getBean(WithDependency.class);
        WithDependency bean = context.getBean(WithDependency.class);
        assertEquals(bean, bean1);
    }

    @Test
    void test_not_in_scope() {
        assertThrows(RuntimeException.class, () -> {
            context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void test_interface() {
        InterfaceImpl bean = (InterfaceImpl) context.getBean(Interface.class);
        assertNotNull(bean);
    }

    @Test
    void test_Return_simple_dependent() {
        SimpleObject bean = (SimpleObject) context.getBean(SimpleInterface.class);
        assertEquals(bean.getSimpleDependent().getName(),"O_o");
    }


    @Test
    void use_the_first_constructor() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean.getDependent());
    }

    @Test
    void test_order_function() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        assertNotNull(bean);
    }
}
