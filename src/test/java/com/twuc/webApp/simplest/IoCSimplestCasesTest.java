package com.twuc.webApp.simplest;

import com.twuc.webApp.simplest.scanning.ObjectWithoutDependency;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

class IoCSimplestCasesTest {
    @Test
    void should_create_object_using_generic_application_context() {
        GenericApplicationContext context = new GenericApplicationContext();
        context.registerBean(ObjectWithoutDependency.class);

        context.refresh();

        ObjectWithoutDependency bean = context.getBean(ObjectWithoutDependency.class);

        assertNotNull(bean);
        assertSame(ObjectWithoutDependency.class, bean.getClass());
    }

    @Test
    void should_create_object_using_annotated_application_context() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
            "com.twuc.webApp.simplest.scanning"
        );

        AnnotatedObjectWithoutDependency bean =
            context.getBean(AnnotatedObjectWithoutDependency.class);

        assertNotNull(bean);
        assertSame(AnnotatedObjectWithoutDependency.class, bean.getClass());
    }
}
